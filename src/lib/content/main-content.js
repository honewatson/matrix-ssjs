import { components, h, state, createMarkup } from "sq";
const { Content } = components;

export const MainContent = ({ title, contents}) => (
  <Content>
    <h2 className="subtitle is-2">{title}</h2>
    <div dangerouslySetInnerHTML={createMarkup(contents)}></div>
  </Content>
);

