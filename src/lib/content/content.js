import { h, preactRender, page, cleanString } from "sq";
import { MainContent } from "./main-content";


print(preactRender(MainContent({
  title: page.state.title,
  contents: cleanString('%globals_asset_contents_raw^rawurlencode%')
})));
