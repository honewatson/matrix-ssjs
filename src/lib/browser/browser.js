export function getHtmlData(id) {
  const result = {};
  const isJson = i => i.getAttribute("data-itype") === "json";
  Array.from(document.getElementById(id).children).forEach(i => {
    const value = isJson(i) ? JSON.parse(i.innerHTML) : i.innerHTML;
    result[i.getAttribute("data-tag")] = value;
  });
  return result;
}
