import { createMarkup } from "./utils";
import { h } from "preact";

export const Xml = ({ data, id, className = "display-none", globalVar = 'sq', el="script" }) => (
  <div>
    <div id={id} className={className}>
      {Object.keys(data).map(tag => {
        const item = data[tag];
        return  (<div data-tag={tag} data-itype={item.type}  dangerouslySetInnerHTML={createMarkup(item.content)}></div>)
      })}
    </div>
    <script>
      (function({globalVar}) {'{'}
        {globalVar}['{id}'] = {globalVar}.getHtmlData('{id}');
      {'}'})({globalVar});
    </script>
  </div>
);
