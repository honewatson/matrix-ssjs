// # General Utility Functions
//
// ## Debug
// Wrap a function or object to see the output in console.
// ### Usage
// ```
// const reduce = data => (debug({url: data.url}))
// ```
// ### Function
export const debug = (obj, trace = false) => {
    console.log(obj);
    if (trace) {
        console.trace();
    }

    return obj;
};

// Base64 encoding/decoding
// https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding

export function b64EncodeUnicode(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
    }));
}

export function b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}


export const maybeBase64Decode = text => {
    if (!text) {
        return '';
    }
    try {
        return b64DecodeUnicode(text);
    } catch (e) {
        return text;
    }
};

export const set = (state, actions, data = {}) =>
    Object.assign(state, data, {
        timestamp: new Date().getTime()
    });

//const DIRTY = /&|<|>|"|script|iframe|=|\(|\)|\[|\]/gi;
//javascript:alert
const DIRTY = /data:text\/html|script|iframe|javascript:|<|>|\(|\)|\[|\]/gi;
export const clean = str => {
    str = str.replace(DIRTY, ' ');
    return str.replace(/'/gi, '&#39;').replace(/"/gi, '&#34;');
};

// ## Immutable Copy
// Create a new version of an object
export const copy = obj => JSON.parse(JSON.stringify(obj));

export const jsonEncode = (obj, empty = {}) => {
    var result;
    try {
        result = JSON.stringify(obj);
    } catch (e) {
        var script = 'script';
        if (typeof isV8 !== 'undefined') {
            print(`<${script}>console.error(${JSON.stringify(e)})</${script}>`);
        } else {
            console.error(e);
        }
        result = JSON.stringify(empty);
    }
    return result;
};

export const jsonDecode = (obj, empty = {}) => {
    var result = empty;
    try {
        result = JSON.parse(obj);
    } catch (e) {
        var script = 'script';
        if (typeof isV8 !== 'undefined') {
            print(`<${script}>console.error(${JSON.stringify(e)})</${script}>`);
        } else {
            console.error(e);
        }
    }
    return result;
};

export const copySafe = (obj, empty = {}) => jsonDecode(jsonEncode(obj, empty));

export const convertMatrixBase64 = content =>
    b64DecodeUnicode(content)
        .replace(/'/gi, '&apos;')
        .replace(/\\\\\\"/gi, "'")
        .replace(/\\/gi, '');


// ## Immutable
// Make a new copy of a value if it is an object and not a function
export const immutable = item =>
    // If is dom event just return as is
    typeof item.target !== 'undefined'
        ? item // If is object and not a function then copy the object
        : typeof item === 'object' && typeof item !== 'function' ? copy(item) : item;

// ## immutableAll
// Create new copies of arguments and return as an array
const immutableAll = (...args) => args.map(immutable);

// ## pipe
// Push result through a series of functions
// ### Usage
// ```js
// const __convert = pipe(function1, function2, function3);
// const result = __convert(...args);
// // function1(...args) -> function2(resultFunction1) -> function3(resultFunction2) -> resultFunction3
// ```
//
// ### Function
export const pipe = (fn, ...fns) => (...args) => {
    return fns.reduce((acc, fn) => {
        return fn(acc);
    }, fn(...immutableAll(...args)));
};

// ## compose
// Classic compose function for functional style programming.  Results are piped through each function from right to left.
// ### Usage
// ```js
// const result = compose(function3, function2, function1);
// // function1(...args) -> function2(resultFunction1) -> function3(resultFunction2) -> resultFunction3
// ```
//
// ### Function
export const compose = (...fns) => pipe(...fns.reverse());

// ## __flatten
// Flattens nested arrays into a flat array.
// ### Usage
// ```js
// const result = __flatten([1, [2, [3, 4, [5, 6, 7]]);
// // [1, 2, 3, 4, 5, 6, 7]
// ```
//
// ### Function
const __flatten = out => list => {
    list.forEach(item => {
        if (Array.isArray(item)) {
            out = __flatten(out)(item);
        } else {
            out = out.concat(item);
        }
    });
    return out;
};

// ## flatten
// Alias for __flatten.  See __flatten for details.
export const flatten = nlist => __flatten([])(nlist);

// ## generateUUID
// Generates a uuid.
// ### Usage
// ```js
// const uuid = generateUUID();
// //
// ```
//
// ### Function
export const generateUUID = () =>
    ('' + 1e7 + -1e3 + -4e3 + -8e3 + -1e11).replace(/1|0/g, function() {
        return (0 | (Math.random() * 16)).toString(16);
    });

// ## r
// Route helper for hyper app router event listener.
// ### Usage
// ```js
// // When the routes '/' or '/search' is activated in window.location then call the function funnelbackRequest.
// app({
//   events: {
//      route: [
//        r('/search', funnelbackRequest),
//        r('/', funnelbackRequest)
//    ],
// });
// ```
//
// ### Function
export const r = (routeMatch, handler) => (state, actions, data) => {
    return data.match === routeMatch ? handler(state, actions, data) : null;
};

// ## htmlDecode
// Decode html entities.
// ### Usage
// ```js
// htmlDecode('&amp; &lt; &#60;');
// // "& < <"
// ```
//
// ### Function
export const htmlDecode = html => {
    var txt = document.createElement('textarea');
    txt.innerHTML = html;
    return txt.value;
};

// ## __replaceParams
// Replace a param in a query string
// ### Usage
// ```js
// // window.location.search === '?facet=blue&query=bingo+jones'
// const result = __replaceParams(location.pathname + location.search)('query', 'hello+world');
// // /search?facet=blue&query=hello+world
// ```
//
// ### Function
export const __replaceParams = str => (param, value) => {
    let qsplit = str.split('?');
    let query = qsplit[1] || '';
    if (query.match(param)) {
        let querySplit = query.split('&');
        let next = true,
            i = 0,
            final = querySplit.length;
        while (next) {
            let item = querySplit[i];
            if (final === i) {
                next = false;
            } else if (item.indexOf(param) !== -1) {
                str = str.replace(item, `${param}=${value}`);
                next = false;
            }
            i++;
        }
        return str;
    } else {
        const newQstring = `${param}=${value}`;
        return query ? str + `${str.substr('-1') == '&' ? '' : '&'}${newQstring}` : str + '?' + newQstring;
    }
};

// ## getURLParameter
// Get the value of a parameter from a query string.
// ### Usage
// ```js
// // location.search == '?query=hello+world'
// const result = getURLParameter('query', location.search);
// // 'hello+world'
// ```
//
// ### Function
export function getUrlParameter(name, url = null) {
    name = name
        .replace(/[\[]/, '\\[')
        .replace(/[\]]/, '\\]')
        .replace(/\./gi, '\\.')
        .replace(/\+/gi, '\\+');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(url ? url : location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

// ## qparts
// Split query param by space
/**
 * Splits the query into words
 * @param query
 */
const qparts = query => query.split(' ');

// ## highlight
// Highlights a word 'qpart' in a string.
// ### Usage
// ```js
// const result = highlight('juice', 'juicer');
// // <strong>juice</strong>r
// ```
//
// ### Function
/**
 * Highlights a word 'qpart' in a string
 * @param qpart
 * @param str
 */
export const highlight = (keyword, str) =>
    keyword ? str.replace(new RegExp(`(${keyword})`, 'gi'), '<strong>$1</strong>') : '';

const __xhighlight = queryParts => strPart => {
    const keyword = queryParts.find(keyword => keyword.length > 2 && strPart.match(new RegExp(`(${keyword})`, 'gi')));
    return keyword ? strPart.replace(new RegExp(`(${keyword})`, 'gi'), '<strong>$1</strong>') : strPart;
};

// ## highlightWords
// Highlights all words in the query against matches in the title
// ### Usage
// ```js
// const result = highlightWords('english', 'happy birthday english');
// // happy birthday <strong>english</strong>
// ```
//
// ### Function
/**
 * Highlights all words in the query against matches in the title
 * @param query
 * @param str
 */
export const highlightWords = (query, str) => {
    if (str) {
        const highlight = __xhighlight(qparts(query));
        return qparts(str)
            .map(highlight)
            .join(' ');
    } else {
        return '';
    }
};

// ## slugify
// Converts a string into something that can be used in a URI with no spaces and funny characters.
// ### Usage
// ```js
// const result = slugify('hello world');
// // hello-world
// ```
//
// ### Function
/**
 * @returns {string}
 */
export const slugify = text => {
    return text
        .toString()
        .toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
};

// ## KeyValue
// Creates a key value object.
// ### Usage
// ```js
// const result = KeyValue('query', 'hello+world');
// // result.key result.value
// ```
//
// ### Function
export class KeyValue {
    constructor(key, value) {
        this.key = key;
        this.value = value;
    }
}

export const keyValue = (key, value) => new KeyValue(key, value);

// ## objToQueryString
// Converts an object to a list of KeyValue objects.
// ### Usage
// ```js
// const result = objToQueryString({query: 'hello+world', category: 'blue'});
// // [KeyValue(hello, world), KeyValue(category, blue)]
// ```
//
// ### Function
export const objToQueryString = obj => Object.keys(obj).map(key => new KeyValue(key, obj[key]));

// ## _parseQuery
// Takes a string and converts to Array<KeyValue>
// ### Usage
// ```js
// const result = _parseQuery('query=hello+world&category=blue');
// // [KeyValue(hello, world), KeyValue(category, blue)]
// ```
//
// ### Function
const _parseQuery = str => str.split('&').map(qpart => new KeyValue(...qpart.split('=')));

// ## mergeQuery
// Takes two queries of type Array<KeyValue> and merges them.
// ### Usage
// ```js
// const result = mergeQuery(
//   objToQueryString({query: '!padrenull', cat: 'blue'}),
//   _parseQuery('query=hello+world')
// );
// // [KeyValue(query, hello+world), KeyValue(cat, blue)]
// ```
//
// ### Function
const mergeQuery = (defaultQuery, query) =>
    defaultQuery
        .filter(param => {
            return !query.find(qparam => qparam.key == param.key);
        })
        .concat(query);

// ## generateQuery
// Generate an Array<KeyValue> from a string and a default Array<KeyValue>.
// ### Usage
// ```js
// const result = generateQuery('query=hello+world', [new KeyValue('query', '!padrenull')]);
// // [KeyValue(query, hello+world)]
// ```
//
// ### Function
const generateQuery = (str, defaultQuery) => mergeQuery(defaultQuery, _parseQuery(str));

// ## parseQuery
// parse a query string using a default query.
// ### Usage
// ```js
// const result = parseQuery('query=hello+world', [new KeyValue('query', '!padrenull')]);
// // [KeyValue(query, hello+world)]
// ```
//
// ### Function
export const parseQuery = (str, defaultQuery = []) =>
    str && str.substr(0, 1) === '?'
        ? generateQuery(str.substr(1), defaultQuery)
        : str ? generateQuery(str, defaultQuery) : defaultQuery;

export const keyValuesToString = keyValues =>
    keyValues.reduce((accum, item) => `${accum}&${item.key}=${item.value}`, '').substr(1);

export const keyValuesToQuery = keyValues => '?' + keyValuesToString(keyValues);

export const addQueryDefaults = (searchQuery, defaults) =>
    keyValuesToQuery(parseQuery(searchQuery, objToQueryString(defaults)));

export const keyValuesToObject = keyValues => {
    const final = {};
    keyValues.forEach(keyValue => {
        final[keyValue.key] = keyValue.value;
    });
    return final;
};

// ## isLocalhost
// Check if is local development server
export const isLocalhost = () => location && location.hostname === 'localhost';

export const createMarkup = markup => ({ __html: markup });

export const reactKey = (namespace, state, index) => `${namespace}-${state.id}-${index}`;

export const capitalize = string => string.charAt(0).toUpperCase() + string.slice(1);

export const isItems = items => Object.keys(items).length;

export const arrayFromSet = set => {
    let final = [];
    set.forEach(item => final.push(item));
    return final;
};

export const filterByKeys = (keys, obj) => {
    const result = {};
    keys.forEach(key => {
        if (obj[key]) {
            result[key] = obj[key];
        }
    });
    return result;
};

export const any = (truthy, list) => list.find(truthy);

export const all = (truthy, list) => list.length === list.filter(truthy).length;

export const keys = obj => Object.keys(obj);

export const range = (fn, count) => [...Array(count).keys()].map(fn);

export const matchProp = (obj, prop, reg) => {
    const bits = prop.split('.');
    var next = obj;
    var accumulate = [];
    while (bits.length) {
        var nextProp = bits.shift();
        next = next[nextProp];
        if (!next) {
            console.warn(
                `Property '${accumulate.join(' -> ')}' does not have property '${nextProp}'\n${JSON.stringify(
                    obj,
                    null,
                    2
                )}`
            );
            console.trace();
            return false;
        }
        accumulate.push(nextProp);
    }
    return next.match(reg);
};

export const rmWhitespace = str => str.trim().replace(/\s+/gi, '');
