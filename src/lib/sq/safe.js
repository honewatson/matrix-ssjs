import { keyValue } from "./utils";

export const cleanString = str => decodeURIComponent(str).replace(/'/gi, "’");

export class SafeObject {
  constructor(values = []) {
    this.values = [];
  }
  decodeKeyValue(key, value, type = null) {
    try {
      value = cleanString(value);
    } catch (e) {
      value = "";
    }
    this.values.push(keyValue(key, type ? ({type, content: value}) : value));
  }
  keyValue(key, value, type = null) {
    this.values.push(keyValue(key, type ? ({type, content: value}) : value));
  }
  toObject() {
    return this.values.reduce((a, it) => {
      a[it.key] = it.value;
      return a;
    }, {});
  }
}

export const printJavascriptData = (data, assign, el = "script") =>
  print(`<${el}>var ${assign} = ${JSON.stringify(data, null, 2)}</${el}>`);

export { keyValue };
