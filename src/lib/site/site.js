import {SafeObject} from 'sq';

const site = new SafeObject();

site.decodeKeyValue('name', '%globals_site_name^rawurlencode%');

const state = site.toObject();

export {
  state
}
