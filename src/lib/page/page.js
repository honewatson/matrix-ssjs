import {SafeObject} from 'sq';

const page = new SafeObject();

page.decodeKeyValue('title', '%frontend_asset_name^rawurlencode%');
page.decodeKeyValue('description', '%frontend_asset_metadata_description^rawurlencode%');


const state = page.toObject();

export {
  state
}
