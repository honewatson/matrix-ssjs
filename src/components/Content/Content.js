import { h } from "sq";

export const Content = ({children}) =>
  <div>{children}</div>;
