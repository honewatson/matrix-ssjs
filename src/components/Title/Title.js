import { h } from "sq";

export const Title = ({ title }) => <h1 className="title is-1">{title}</h1>;

