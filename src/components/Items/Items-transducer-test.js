import zora from "zora";
import { itemsTransducer } from "./Items-transducer";

const describe = zora.test;

describe("itemsTransducer()", t => {
  const data = {
    items: [{ "asset_metadata_page.title^rawurlencode": "Hello%20World" }]
  };
  const result = itemsTransducer(data);
  t.test(
    "should convert `asset_metadata_page.title^rawurlencode` to title",
    t => {
      t.ok(result.items[0].title === "Hello World");
    }
  );
});
