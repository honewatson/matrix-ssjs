import { cleanString } from "../../lib/sq/safe";

export const itemsTransducer = (
  data = {
    items: [{ "asset_metadata_page.title^rawurlencode": "Hey%20Buddy" }]
  }
) => ({
  ...data,
  items: data.items.reduce((a, it) => {
    return a.concat({
      ...it,
      title: cleanString(it["asset_metadata_page.title^rawurlencode"])
    });
  }, [])
});
