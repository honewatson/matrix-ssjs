const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');

const join = path.join;

const createFileHard = (target, content) => {
  console.log(target);
  console.log(content);
  fs.writeFileSync(target, content);
};

createFileHard(
  'src/lib/sq/version.js',
  `export const version = '${new Date().toJSON().replace(/:/gi, '')}';
export const env = '${process.env.NODE_ENV || 'dev'}';
`
);
