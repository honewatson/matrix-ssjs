const rollup = require("rollup");
const rollupNodeResolve = require("rollup-plugin-node-resolve");
const rollupCommonjs = require("rollup-plugin-commonjs");
const rollupBabel= require("rollup-plugin-babel");
const rollupPluginJson = require("rollup-plugin-json");
const rollupMultiEntry = require("../framework/for-import/rollup-multi-entry-out");
const rollupPrettier = require("rollup-plugin-prettier");
const rollupScss = require("rollup-plugin-scss");
const { yellow, red, blue, bold } = require("colorette");
const gitStatus = require("git-status");
const rollupHtml = require("rollup-plugin-html");
const { exec } = require("child_process");
const path = require("path");

const doRollup = input =>
  !input.match(/dist|static|nunjucks/)
    ? rollup
        .rollup({
          input: input,
          onwarn: function(message) {
            console.log(`Notice... ${yellow(message.toString())}`);
          },
          plugins: [
            rollupMultiEntry(),
            rollupScss({ output: false }),
            rollupHtml(),
            rollupPluginJson(),
            rollupNodeResolve({
              browser: true,
              main: true
            }),
            rollupCommonjs({ include: "node_modules/**" }),
            rollupBabel(),
            rollupPrettier({})
          ],
          external: [],
          globals: {
            window: "window"
          }
        })
        .then(bundle =>
          bundle.generate({
            format: "cjs"
          })
        )
        .then(function outputContent(result) {
          console.log(`Successful build of ${input}!`);
          exec(
            `node_modules/.bin/prettier --jsx-bracket-same-line --single-quote  --write ${input}`,
            (error, stdout, stderr) => {
              if (error) {
                console.error(`Prettier error: ${error}`);
                return;
              }
              console.log(`Prettier: ${stdout}`);
              if (stderr) {
                console.log(`Prettier error: ${stderr}`);
              }
            }
          );
          //node_modules/.bin/prettier --jsx-bracket-same-line --print-width 120 --write
        })
        .catch(e => {
          console.log(red(`Failed build of ${input}!`).toString());
          console.log(`${red(e)}`, e);
          if (e.id) {
            console.log(`${red(e.id)}`);
          }
        })
    : console.log("Ignoring... " + input);

const runGitStatus = () =>
  gitStatus((err, data) => {
    if (err) console.error(err);
    else {
      data
        .filter(item => item.y == "M" || item.y == "U" || item.x == "M")
        .forEach(item => {
          if (item.to.match(/.js$/)) {
            console.log(`Will try and build ${yellow(item.to)}!`);
            doRollup(item.to);
          }
        });
    }
    // => [ { x: ' ', y: 'M', to: 'example/index.js', from: null } ]
  });

const getPath = filepath =>
  filepath.match(/\.js|\*/) ? filepath : `${filepath}{/*.js,/**/*.js}`;

if (require.main === module) {
  if (process.argv.length > 2) {
    const Taskr = require("taskr");
    const d = getPath(process.argv[2]);
    const taskr = new Taskr({
      tasks: {
        *main(task) {
          yield task.source(d).run({
            every: true,
            *func(file) {
              doRollup(path.join(file.dir, file.base));
            }
          });
        }
      }
    });
    taskr.start("main");
  } else {
    runGitStatus();
  }
}

module.exports = {
  runGitStatus: runGitStatus,
  doRollup: doRollup
};
