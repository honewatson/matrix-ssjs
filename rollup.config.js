import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import babel from "rollup-plugin-babel";
import json from "rollup-plugin-json";
import pkg from "./package.json";

const ROLLUP_FILE = process.env.ROLLUP_FILE;
const ROLLUP_DIST = process.env.ROLLUP_DIST;
const ROLLUP_BASE = process.env.ROLLUP_BASE;
const input = `src/lib/${ROLLUP_FILE}/${ROLLUP_FILE}.js`;
const filePath = (file, pkgFile = "") =>
  `${ROLLUP_DIST}/${pkgFile}${ROLLUP_FILE}.${file}.js`;
const pkgFile = ROLLUP_BASE ? "" : `${pkg.libNamespace}.`;

function matrix() {
  return {
    name: "matrix",
    renderChunk(code) {
      console.log(code);
      return code.replace(/'%%%/gi, "%").replace(/%%%'/gi, "%");
    }
  };
}

export default [

  // CommonJS (for Node) and ES module (for bundlers) build.
  // (We could have three entries in the configuration array
  // instead of two, but it's quicker to generate multiple
  // builds from a single configuration where possible, using
  // an array for the `output` option, where we can specify
  // `file` and `format` for each target)
  {
    input,
    plugins: [
      babel(),
    ],
    output: [
      { file: filePath("es", pkgFile), format: "es" }
    ]
  },
  // browser-friendly UMD build
  {
    input,
    output: {
      name: ROLLUP_BASE ? ROLLUP_BASE : `${pkg.libNamespace}.${ROLLUP_FILE}`,
      file: filePath("umd", pkgFile),
      format: "umd"
    },
    plugins: [
      babel(),
      resolve(), // so Rollup can find `ms`
      commonjs(), // so Rollup can convert `ms` to an ES module,
      matrix(),
      json()

    ]
  },
];
