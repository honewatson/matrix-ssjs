const pkg = require("./package.json");
const execa = require("execa");
const path = require("path");
const { red, blue, bold } = require("colorette");
const dist = process.env.ENV === "production" ? "static" : "dist";
const join = path.join;
const lib = `src/lib/**/*.js`;

const envData = src => {
  const list = src.split("/");
  if (list.length > 2) {
    const one = list.shift();
    const two = list.shift();
    const three = list.shift();
    if (one === "src" && two === "lib") {
      var ROLLUP_BASE = "";
      if (pkg.libNamespace === three) {
        ROLLUP_BASE = three;
      }
      const ROLLUP_DIST = dist;
      const ROLLUP_FILE = three;
      return {
        ROLLUP_DIST,
        ROLLUP_FILE,
        ROLLUP_BASE
      };
    } else {
      return {};
    }
  } else {
    return {};
  }
};

/**
 * Simple Taskr Copy Generator Factory
 * @param source
 * @param target
 * @returns {copy}
 */
const copy = (source, target) => {
  return function* copy(task) {
    yield task.source(source).target(target);
  };
};

function* libWatch(task, o) {
  const rollupData = envData(o.src);
  if (rollupData.ROLLUP_FILE) {
    const EXPORTS = Object.keys(rollupData).reduce((a, it) => {
      const result = `export ${it}="${rollupData[it]}";`;
      return a + result;
    }, "");
    const comm = `${EXPORTS}node_modules/.bin/rollup -c`;
    task.$.log("Build", bold(blue(`${rollupData.ROLLUP_FILE}.js`)));
    yield task.source(o.src).shell(comm); //.dist('dist');
  }
}

function* libBuild(task) {
  yield task
    .source(lib)
    .run({
      every: false,
      *func(files) {
        files.forEach(f => {
          const rollupData = envData(join(f.dir, f.base));
          if (rollupData.ROLLUP_FILE && f.base === `${rollupData.ROLLUP_FILE}.js`) {
            const EXPORTS = Object.keys(rollupData).reduce((a, it) => {
              const result = `export ${it}="${rollupData[it]}";`;
              return a + result;
            }, "");
            const comm = `${EXPORTS}node_modules/.bin/rollup -c`;
            task.$.log("Build", bold(blue(`${rollupData.ROLLUP_FILE}.js`)));

            (async () => {
              try {
                const { stdout } = await execa.shell(comm);
                task.$.log(
                  `Finished building`,
                  bold(blue(`${rollupData.ROLLUP_FILE}.js`))
                );
              } catch (e) {
                task.$.log(e.message);
              }
            })();
            //yield task.source(o.src).shell(comm);//.dist('dist');
          }
        });
      }
    })
    .target("dist");
}

function* watch(task) {
  yield task.watch(lib, ["libWatch"]);
}

function* build(task) {
  yield task.parallel(["libBuild", "vendorCopy", "htmlCopy"]);
}

module.exports = {
  watch,
  build,
  libBuild,
  libWatch,
  vendorCopy: copy("./vendor/*.*", `${dist}/vendor`),
  htmlCopy: copy("./src/html/*.html", `${dist}/html`)
};
